/*
Navicat MySQL Data Transfer

Source Server         : root
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : wordsplit

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2017-04-07 09:57:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_sentence
-- ----------------------------
DROP TABLE IF EXISTS t_sentence;
CREATE TABLE t_sentence (
  id int(8) NOT NULL AUTO_INCREMENT,
  sentence varchar(600) NOT NULL COMMENT '原句',
  accept_words varchar(1000) DEFAULT NULL COMMENT '已确认词组',
  is_accept int(1) DEFAULT '0' COMMENT '是否已确认',
  accept_time datetime DEFAULT NULL COMMENT '确认时间',
  assigned_num int(8) DEFAULT '0' COMMENT '被分配次数',
  submit_num int(8) DEFAULT '0' COMMENT '提交次数',
  state int(1) DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS t_user;
CREATE TABLE t_user (
  user_id int(8) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  username varchar(50) NOT NULL COMMENT '用户名',
  password varchar(50) NOT NULL COMMENT '密码',
  create_time datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  login_count int(8) DEFAULT '0' COMMENT '登录次数',
  last_login_time datetime DEFAULT NULL COMMENT '最后登录时间',
  split_num int(8) DEFAULT '0' COMMENT '已分词次数',
  accepted_num int(8) DEFAULT '0' COMMENT '被采纳次数',
  auth int(1) DEFAULT '0',
  PRIMARY KEY (user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_word
-- ----------------------------
DROP TABLE IF EXISTS t_word;
CREATE TABLE t_word (
  id int(8) NOT NULL AUTO_INCREMENT,
  sentence_id int(8) NOT NULL COMMENT '原句id',
  words varchar(1000) NOT NULL COMMENT '词组',
  split_time datetime DEFAULT CURRENT_TIMESTAMP COMMENT '分词时间',
  split_user int(8) NOT NULL COMMENT '分词人',
  state int(1) DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
