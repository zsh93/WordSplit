/**
 * Created by ZhaoShihao on 2017/4/8 0008.
 */
var detail = {
    URL: {
        submitUrl: function (rc) {
            return rc + "/submit";
        },
        sentenceUrl: function (rc) {
            return rc + "/getSentence";
        },
        editPswUrl: function (rc) {
            return rc + "/editPsw";
        }
    },
    init: function (rc) {
        $("#submit").click(function () {
            detail.submit(rc);
        });
        $("#next").click(function () {
            detail.next(rc);
        });
        $("#submitPwd").click(function () {
            detail.editPsw(rc);
        });
        detail.getSentence(rc);
    },
    editPsw: function (rc) {
        var newPwd = $("#newPwd").val();
        var newPwd2 = $("#newPwd2").val();
        var oldPwd = $("#oldPwd").val();
        var url = detail.URL.editPswUrl(rc);
        if (newPwd == newPwd2) {
            $.post(url, {
                oldPwd: oldPwd,
                newPwd: newPwd
            }, function (res) {
                if (res == 1) {
                    alert("修改成功");
                    $("#oldPwd").val("");
                    $("#newPwd").val("");
                    $("#newPwd2").val("");
                    $('#modalPwd').modal('hide');
                } else {
                    detail.alertError("alertPwd", "原密码输入错误或系统异常");
                }
            });
        } else {
            detail.alertError("alertPwd", "两次密码输入不一致");
        }
    },
    canSubmit: function () {
        var sentence = $("#sentence").text();
        var words = $("#words").val();
        if (sentence != words) {
            $("#submit").attr("disabled", false);
        } else {
            $("#submit").attr("disabled", true);
        }
    },
    alertError: function (id, msg) {
        $("#" + id + "Text").text(msg);
        $('#' + id).show();
        window.setTimeout(function () {
            $("#" + id + "Text").text("");
            $('#' + id).hide();
        }, 3000);
    },
    submit: function (rc) {
        var sentenceId = $("#sentenceId").val();
        var words = $("#words").val();
        var url = detail.URL.submitUrl(rc);
        if (words.indexOf('$') < 0) {
            detail.alertError("alert", "请使用指定分隔符进行分词！");
            $("#words").val($("#sentence").text());
            return;
        }
        $.post(url, {
            sentenceId: sentenceId,
            words: words
        }, function (res) {
            if (res) {
                detail.flushUser(res);
                detail.getSentence(rc);
            } else {
                detail.alertError("alert", "系统异常，请稍候重试！");
            }
        });
    },
    flushUser: function (user) {
        $("#split").text(user.splitNum);
        $("#accepted").text(user.acceptedNum);
    },
    next: function (rc) {
        detail.timer("next", 5, "换一条");
        detail.getSentence(rc);
    },
    getSentence: function (rc) {
        var count = 50;
        var url = detail.URL.sentenceUrl(rc);
        $.post(url, {
            count: count
        }, function (res) {
            if (res) {
                if (res.id == -1) {
                    $("#sentence").text("已无更多可分词原句");
                    $("#words").attr("disabled", true);
                    $("#next").attr("disabled", true);
                    $("#submit").attr("disabled", true);
                    $("#sentenceId").val(-1);
                    return;
                }
                $("#sentenceId").val(res.id);
                $("#sentence").text(res.sentence);
                $("#words").val(res.sentence);
            } else {
                detail.alertError("alert", "系统异常，请稍候重试！");
            }
        });
    },
    timer: function (id, time, text) {
        var btn = $("#" + id);
        btn.attr("disabled", true); //按钮禁止点击
        var hander = setInterval(function () {
            if (time <= 0) {
                clearInterval(hander); //清除倒计时
                btn.text(text);
                id = $("#sentenceId").val();
                if (id > 0) {
                    btn.attr("disabled", false);
                }
                return;
            } else {
                btn.html("" + time--);
            }
        }, 1000);
    }
};