<%@ page language="java" import="java.util.*"
         contentType="text/html; charset=UTF-8" errorPage="ErrorView.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>请登录</title>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/static/css/Login.css"/>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/static/css/body.css"/>
</head>
<body>
<div class="container">
    <section id="content">
        <form action="${pageContext.request.contextPath}/login"
              method="post">
            <h1>分词登录</h1>
            <div>
                <input type="text" placeholder="请输入账号" required="" id="username"
                       name="username"/>
            </div>
            <div>
                <input type="password" placeholder="请输入密码" required="" id="password"
                       name="password"/>
            </div>
            <div class="">
                ${err} <span class="help-block u-errormessage"
                                          id="js-server-helpinfo"></span>
            </div>
            <div>
                <!-- <input type="submit" value="Log in" /> -->
                <input type="submit" value="登录" class="btn btn-primary"
                       id="js-btn-login"/>
            </div>
        </form>
    </section>
</div>
</body>
</html>