<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>分词详情页</title>
    <%@include file="common/head.jsp" %>
</head>
<body>
<div class="container">
    <div class="panel panel-default text-center">
        <div class="panel-heading">
            <span style="float: right">
            ${sessionScope.user.username}，您好！
                <a href="javascript:void(0)" data-toggle="modal" data-target="#modalPwd" class="text-danger">[修改密码]</a>
                <a href="${pageContext.request.contextPath}/exit" class="text-danger">[退出登录]</a><br/>
            </span>
            <h1>欢迎使用分词协同系统</h1>
            <h4>
                <strong class="text-primary">
                    截止到目前为止，
                    您共分词<span class="text" id="split" style="margin:0 5px">${sessionScope.user.splitNum}</span>个，
                    被采纳了<span class="text" id="accepted" style="margin:0 5px">${sessionScope.user.acceptedNum}</span>个
                    <%--<a href="#" style="margin-left: 20px">[查看详情]</a>--%>
                </strong>
            </h4>
        </div>
        <!-- 模态框（Modal） -->
        <div class="modal fade" id="modalPwd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width: 30%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title" id="myModalLabel">
                            修改密码
                        </h4>
                    </div>
                    <div class="modal-body">
                        <input class="form-control" style="margin:5px 0;" placeholder="请输入原密码" id="oldPwd" type="password"/>
                        <input class="form-control" style="margin:5px 0;" placeholder="请输入新密码" id="newPwd" type="password"/>
                        <input class="form-control" style="margin:5px 0;" placeholder="请再次输入新密码" id="newPwd2" type="password"/>
                        <div id="alertPwd" class="alert alert-danger in" style="padding: 5px; margin:10px 0;display: none">
                            <strong>警告!</strong><span id="alertPwdText"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                        </button>
                        <button type="button" id="submitPwd" class="btn btn-primary">
                            提交更改
                        </button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal -->
        </div>
        <div class="panel-body" style="padding: 0 10% 0 10%">

            <h2 class="text-danger">
                <span id="sentence">等待原句...</span>
            </h2>
            <h3 class="text-warning">
                <small>请输入分词结果（请按照语序并使用“$”进行分割）：</small>
                <br/>
            </h3>
            <div id="alert" class="alert alert-danger in" style="padding: 5px; margin:10px 5%;display: none">
                <strong>警告!</strong><span id="alertText"></span>
            </div>
            <form method="post" action="#" class="form-inline" style="margin-bottom: 20px">
                <input class="form-control" id="words" style="width: 70%" name="words" type="text"
                       onkeyup="detail.canSubmit()"/>
                <input type="hidden" id="sentenceId">
                <a class="btn btn-success" disabled="true" style="width: 10%" id="submit">
                    提交
                </a>
                <a class="btn btn-danger" style="width: 10%" id="next">
                换一条
                </a>
            </form>
        </div>
    </div>
</div>


</body>
<!-- jQuery (Bootstrap 的 JavaScript 插件需要引入 jQuery) -->
<script src="${pageContext.request.contextPath}/static/js/jquery-3.0.0.min.js"></script>
<!-- 包括所有已编译的插件 -->
<script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js"></script>
<!--Cookie-->
<script src="${pageContext.request.contextPath}/static/js/cookie-1.4.1.min.js"></script>
<!--倒计时-->
<script src="${pageContext.request.contextPath}/static/js/countdown-2.1.0.min.js"></script>
<script src="${pageContext.request.contextPath}/static/js/detail.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        detail.init("${pageContext.request.contextPath}");
    });
</script>
</html>