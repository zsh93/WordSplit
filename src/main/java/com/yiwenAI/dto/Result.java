package com.yiwenAI.dto;

/**
 * @author ZhaoShihao
 * @version 1.0
 * @create 2017-03-31 17:54
 */
public class Result {
    private Integer state;
    private Object data;

    @Override
    public String toString() {
        return "Result{" +
                "state=" + state +
                ", data=" + data +
                '}';
    }

    public Result(Integer state) {
        this.state = state;
    }

    public Result(Integer state, Object data) {
        this.state = state;
        this.data = data;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
