package com.yiwenAI.entity;

import java.util.Date;

/**
 * @author ZhaoShihao
 * @version 1.0
 * @create 2017-04-07 10:14
 */
public class Word {
    private Integer id;
    private Integer sentenceId;
    private String words;
    private Date splitTime;
    private Integer splitUser;
    private Integer wordsNum;
    private String state;



    public Word() {
    }

    public Word(String words, Integer sentenceId) {
        this.words = words;
        this.sentenceId = sentenceId;
    }

    @Override
    public String toString() {
        return "Word{" +
                "id=" + id +
                ", sentenceId=" + sentenceId +
                ", words='" + words + '\'' +
                ", splitTime=" + splitTime +
                ", splitUser=" + splitUser +
                ", wordsNum=" + wordsNum +
                ", state='" + state + '\'' +
                '}';
    }

    public Integer getWordsNum() {
        return wordsNum;
    }

    public void setWordsNum(Integer wordsNum) {
        this.wordsNum = wordsNum;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSentenceId() {
        return sentenceId;
    }

    public void setSentenceId(Integer sentenceId) {
        this.sentenceId = sentenceId;
    }

    public String getWords() {
        return words;
    }

    public void setWords(String words) {
        this.words = words;
    }

    public Date getSplitTime() {
        return splitTime;
    }

    public void setSplitTime(Date splitTime) {
        this.splitTime = splitTime;
    }

    public Integer getSplitUser() {
        return splitUser;
    }

    public void setSplitUser(Integer splitUser) {
        this.splitUser = splitUser;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
