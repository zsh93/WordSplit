package com.yiwenAI.entity;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;
import java.util.List;

/**
 * @author ZhaoShihao
 * @version 1.0
 * @create 2017-04-07 10:14
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Sentence {
    private Integer id;
    private String sentence;
    private String acceptWords;
    private Integer isAccept;
    private Date acceptTime;
    private Integer assignedNum;
    private Integer submitNum;
    private Integer state;
    private List<Word> wordList;

    public Sentence(Integer id, String acceptWords) {
        this.id = id;
        this.acceptWords = acceptWords;
    }

    public Sentence() {
    }

    @Override
    public String toString() {
        return "Sentence{" +
                "id=" + id +
                ", sentence='" + sentence + '\'' +
                ", acceptWords='" + acceptWords + '\'' +
                ", isAccept=" + isAccept +
                ", acceptTime=" + acceptTime +
                ", assignedNum=" + assignedNum +
                ", submitNum=" + submitNum +
                ", state=" + state +
                ", wordList=" + wordList +
                '}';
    }

    public List<Word> getWordList() {
        return wordList;
    }

    public void setWordList(List<Word> wordList) {
        this.wordList = wordList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public String getAcceptWords() {
        return acceptWords;
    }

    public void setAcceptWords(String acceptWords) {
        this.acceptWords = acceptWords;
    }

    public Integer getIsAccept() {
        return isAccept;
    }

    public void setIsAccept(Integer isAccept) {
        this.isAccept = isAccept;
    }

    public Date getAcceptTime() {
        return acceptTime;
    }

    public void setAcceptTime(Date acceptTime) {
        this.acceptTime = acceptTime;
    }

    public Integer getAssignedNum() {
        return assignedNum;
    }

    public void setAssignedNum(Integer assignedNum) {
        this.assignedNum = assignedNum;
    }

    public Integer getSubmitNum() {
        return submitNum;
    }

    public void setSubmitNum(Integer submitNum) {
        this.submitNum = submitNum;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}
