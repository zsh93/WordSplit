package com.yiwenAI.entity;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;

/**
 * @author ZhaoShihao
 * @version 1.0
 * @create 2017-04-07 10:14
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class User {
    private Integer userId;
    private String username;
    private String password;
    private Date createTime;
    private Integer loginNum;
    private Date lastLoginTime;
    private Integer splitNum;
    private Integer acceptedNum;
    private Integer auth;

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", createTime=" + createTime +
                ", loginNum=" + loginNum +
                ", lastLoginTime=" + lastLoginTime +
                ", splitNum=" + splitNum +
                ", acceptedNum=" + acceptedNum +
                ", auth=" + auth +
                '}';
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getLoginNum() {
        return loginNum;
    }

    public void setLoginNum(Integer loginNum) {
        this.loginNum = loginNum;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public Integer getSplitNum() {
        return splitNum;
    }

    public void setSplitNum(Integer splitNum) {
        this.splitNum = splitNum;
    }

    public Integer getAcceptedNum() {
        return acceptedNum;
    }

    public void setAcceptedNum(Integer acceptedNum) {
        this.acceptedNum = acceptedNum;
    }

    public Integer getAuth() {
        return auth;
    }

    public void setAuth(Integer auth) {
        this.auth = auth;
    }
}
