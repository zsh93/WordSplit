package com.yiwenAI.util;

import com.yiwenAI.entity.Sentence;
import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author ZhaoShihao
 * @version 1.0
 * @create 2017-04-05 14:18
 */
public class ReadExcel {
    //总行数
    private int totalRows = 0;
    //总条数
    private int totalCells = 0;
    //错误信息接收器
    private String errorMsg;

    //构造方法
    public ReadExcel() {
    }

    //获取总行数
    public int getTotalRows() {
        return totalRows;
    }

    //获取总列数
    public int getTotalCells() {
        return totalCells;
    }

    //获取错误信息
    public String getErrorInfo() {
        return errorMsg;
    }

    public static List<Sentence> read(MultipartFile multipartFile, boolean isSave, String savePath) {
        File file = new File("/" + multipartFile.getName());
        try {
            multipartFile.transferTo(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return read(file, isSave, savePath);
    }

    public static List<Sentence> read(File file, boolean isSave, String savePath) {
        ReadExcel readExcel = new ReadExcel();
        return readExcel.getListByExcel(file, isSave, savePath);
    }

    /**
     * 验证EXCEL文件
     *
     * @param filePath
     * @return
     */
    private boolean validateExcel(String filePath) {
        if (filePath == null || !(isExcel2003(filePath) || isExcel2007(filePath))) {
            errorMsg = "文件名不是excel格式";
            return false;
        }
        return true;
    }

    /**
     * 保存文件
     *
     * @param f
     * @param savePath
     */
    public void saveFile(File f, String savePath) {
        File file = new File(savePath);
        //创建一个目录 （它的路径名由当前 File 对象指定，包括任一必须的父路径。）
        if (!file.exists()) {
            boolean mkdirs = file.mkdirs();
        }
        //新建一个文件
        File file1 = new File(savePath + new Date().getTime() + ".xlsx");
        //将上传的文件写入新建的文件中
        try {
            FileUtils.copyFile(f, file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 读EXCEL文件，获取用户信息集合
     *
     * @param file
     * @return
     */
    public List<Sentence> getListByExcel(File file, boolean isSave, String savePath) {
        if (isSave) {
            saveFile(file, savePath);
        }
        //初始化用户信息的集合
        List<Sentence> sentences = new ArrayList<>();
        //初始化输入流
        FileInputStream is = null;
        String fileName = file.getName();
        try {
            //验证文件名是否合格
            if (!validateExcel(fileName)) {
                return null;
            }
            //根据文件名判断文件是2003版本还是2007版本
            boolean isExcel2003 = true;
            if (isExcel2007(fileName)) {
                isExcel2003 = false;
            }
            is = new FileInputStream(file);
            //根据excel里面的内容读取用户信息
            sentences = getExcelInfo(is, isExcel2003);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    is = null;
                    e.printStackTrace();
                }
            }
            boolean delete = file.delete();
        }
        return sentences;
    }

    /**
     * 根据excel里面的内容读取用户信息
     *
     * @param is          输入流
     * @param isExcel2003 excel是2003还是2007版本
     * @return
     * @throws IOException
     */
    private List<Sentence> getExcelInfo(FileInputStream is, boolean isExcel2003) {
        List<Sentence> sentenceList = null;
        try {
            /** 根据版本选择创建Workbook的方式 */
            Workbook wb = null;
            //当excel是2003时
            if (isExcel2003) {
                wb = new HSSFWorkbook(is);
            } else {//当excel是2007时
                wb = new XSSFWorkbook(is);
            }
            //读取Excel里面用户的信息
            sentenceList = readExcelValue(wb);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sentenceList;
    }

    /**
     * 读取Excel里面用户的信息
     *
     * @param wb
     * @return
     */
    private List<Sentence> readExcelValue(Workbook wb) {
        //得到第一个shell
        Sheet sheet = wb.getSheetAt(0);

        //得到Excel的行数
        this.totalRows = sheet.getPhysicalNumberOfRows();

        //得到Excel的列数(前提是有行数)
        if (totalRows >= 1 && sheet.getRow(0) != null) {
            this.totalCells = sheet.getRow(0).getPhysicalNumberOfCells();
        }
        List<Sentence> sentenceList = new ArrayList<Sentence>();
        Sentence sentence;
        //循环Excel行数,从第二行开始。标题不入库
        for (int r = 1; r < totalRows; r++) {
            Row row = sheet.getRow(r);
            if (row == null) continue;
            sentence = new Sentence();
            //循环Excel的列
            for (int c = 0; c < this.totalCells; c++) {
                Cell cell = row.getCell(c);
                if (null != cell) {
                    switch (c) {
                        case 0: //第一列不读
                            break;
                        case 1:
                            sentence.setSentence(cell.getStringCellValue());//用户工号
                            break;
                    }
                }
            }
            //添加用户
            sentenceList.add(sentence);
        }
        return sentenceList;
    }

    /**
     * 是否是2003的excel，返回true是2003
     *
     * @param filePath
     * @return
     */
    private static boolean isExcel2003(String filePath) {
        return filePath.matches("^.+\\.(?i)(xls)$");
    }

    /**
     * 是否是2007的excel，返回true是2007
     *
     * @param filePath
     * @return
     */
    private static boolean isExcel2007(String filePath) {
        return filePath.matches("^.+\\.(?i)(xlsx)$");
    }
}
