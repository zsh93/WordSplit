package com.yiwenAI.util;

/**
 * @author zhaoshihao
 * @create 2017-03-27 上午10:53
 **/
public class StringUtil {
    /**
     * 判断字符串为空
     *
     * @param str
     * @return
     */
    public static boolean isEmpty(String str) {
        return str == null || str.trim().equals("");
    }

    /**
     * 判断字符串不为空
     *
     * @param str
     * @return
     */
    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }

    /**
     * 删除字符串前后多余双引号
     *
     * @param string
     * @return
     */
    public static String toClearStr(String string) {
        if (isEmpty(string)) return string;
        string = string.trim();
        if (string.startsWith("\"") && string.endsWith("\"")) {
            string = string.substring(1, string.length() - 1);
        } else if (string.startsWith("\"")) {
            string = string.substring(1);
        } else if (string.endsWith("\"")) {
            string = string.substring(0, string.length() - 1);
        }
        return string.trim();
    }

    public static String getPassword(String password) {
        return MD5.encode("pwd" + password + "|" + password.length());
    }

    public static void main(String[] args) {
        String password = getPassword("123456");
        System.out.println(password);
    }
}
