package com.yiwenAI.service;

import com.yiwenAI.entity.Sentence;
import com.yiwenAI.entity.User;

/**
 * @author ZhaoShihao
 * @version 1.0
 * @create 2017-04-07 10:36
 */
public interface UserService {

    User login(String username, String password);

    Sentence getOneSentence(Integer userId, Integer count);

    User submitWords(Integer userId, Integer sentenceId, String words);

    int updatePwd(Integer userId,String oldPwd,String newPwd);
}
