package com.yiwenAI.dao;

import com.yiwenAI.entity.Sentence;
import com.yiwenAI.entity.Word;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 *
 * @author ZhaoShihao
 * @version 1.0
 * @create 2017-04-07 10:32
 */
public interface SentenceDao {
    int insertSentence(List<Sentence> list);

    int deleteSentence(Integer id);

    int updateSentence(Sentence sentence);

    List<Sentence> selectSentenceList(@Param("start") int start, @Param("size") int size, @Param("type") int type);

    Sentence selectSentenceById(Integer id);

    Sentence selectSentenceByRandom(@Param("userId") Integer userId,@Param("count") Integer count);

    /**
     *
     * @param name assigned_num submit_num
     * @param id
     */
    void addSentenceCount(@Param("name") String name,@Param("id") Integer id);

    //todo Sentence selectSentenceByCheck();

    /**
     * 插入新结果
     * @param word
     * @return
     */
    int insertWords(Word word);

    /**
     * 插入关联
     * @param userId
     * @param wordId
     * @return
     */
    int insertUserWord(@Param("userId") Integer userId,@Param("wordId") Integer wordId);

    /**
     * 更新该结果一致总次数
     * @param words
     * @return
     */
    int updateWordsNum(String words);

    /**
     * 查询该结果一致总次数
     * @param words
     * @return
     */
    Word selectWordsNum(String words);
}
