package com.yiwenAI.dao;

import com.yiwenAI.entity.Term;

/**
 * @author ZhaoShihao
 * @version 1.0
 * @create 2017-04-07 10:33
 */
public interface TermDao {
    int insertTerm(Term term);

}
