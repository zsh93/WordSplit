package com.yiwenAI.dao;

import com.yiwenAI.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author ZhaoShihao
 * @version 1.0
 * @create 2017-04-07 10:31
 */
public interface UserDao {
    int insertUser(User user);

    int deleteUser(Integer userId);

    /**
     *
     * @param name split_num accepted_num
     * @param userId
     */
    void addUserCount(@Param("name") String name,@Param("userId") Integer userId);

    int updateUserPassword(@Param("userId")Integer userId, @Param("newPsw") String newPsw, @Param("oldPsw") String oldPsw);

    int updateUserForLogin(Integer userId);

    int updateUserById(User user);

    User selectUserForLogin(@Param("username") String username, @Param("password") String password);

    User selectUserById(Integer userId);

    List<User> selectUserList(@Param("start") int start, @Param("size") int size);
}
