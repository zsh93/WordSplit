package com.yiwenAI.controller;

import com.yiwenAI.entity.Sentence;
import com.yiwenAI.entity.User;
import com.yiwenAI.service.UserService;
import com.yiwenAI.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * @author ZhaoShihao
 * @version 1.0
 * @create 2017-04-07 16:26
 */
@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping("/toDetail")
    public String toDetail(HttpSession session) {
        return session.getAttribute("user") == null ? "redirect:/toLogin" : "detail";
    }

    @RequestMapping("/toLogin")
    public String toLogin() {
        return "forward:/LoginView.jsp";
    }

    @RequestMapping("/toList")
    public String toList() {
        return "list";
    }

    @RequestMapping("/toError")
    public String toError() {
        return "forward:/ErrorView.jsp";
    }

    @RequestMapping("/login")
    public String login(String username, String password, Model model, HttpSession session) {
        try {
            User user = null;
            if (StringUtil.isNotEmpty(username) && StringUtil.isNotEmpty(username)) {
                user = userService.login(username, password);
            }
            if (user != null) {
                session.setAttribute("user", user);
            } else {
                model.addAttribute("err", "用户名或密码错误！");
                return "forward:toLogin";
            }
            return "redirect:toDetail";
        } catch (Exception e) {
            e.printStackTrace();
            return "redirect:toError";
        }
    }

    @RequestMapping("/getSentence")
    @ResponseBody
    public Sentence getSentence(HttpSession session, Integer count) {
        User user = (User) session.getAttribute("user");
        Sentence oneSentence = null;
        if (user != null) {
            oneSentence = userService.getOneSentence(user.getUserId(), count);
        }
        return oneSentence;
    }

    @RequestMapping("/submit")
    @ResponseBody
    public User submit(HttpSession session, Integer sentenceId, String words) {
        User user = (User) session.getAttribute("user");
        if (user != null) {
            user = userService.submitWords(user.getUserId(), sentenceId, words);
            session.setAttribute("user", user);
        }
        return user;
    }

    @RequestMapping("/editPsw")
    @ResponseBody
    public int editPsw(HttpSession session, String oldPwd, String newPwd) {
        int i = 0;
        User user = (User) session.getAttribute("user");
        if (user != null) {
            Integer userId = user.getUserId();
            i = userService.updatePwd(userId, oldPwd, newPwd);
        }
        return i;
    }

    @RequestMapping("/exit")
    public String exit(HttpSession session) {
        session.removeAttribute("user");
        session.invalidate();
        return "redirect:/toLogin";
    }
}
